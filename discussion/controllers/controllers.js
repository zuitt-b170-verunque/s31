// this contains the functions/statements/logic to be executed once a route has been triggered/entered
// responsible for execution of CRUD operations/methods

// to give access to the contents of tasks.js file in the models folder, so it can use a specific model named Task
// const Task = require("../models/tasks.js")
   const task = require("../models/task.js");
const Task = require("../models/task.js");

// create controller function that responds to the routes

module.exports.getAllTasks = () => {
    return Task.find({}).then(result =>{
        return result;
    })
}

module.exports.createTask = (requestBody) => {
    let newTask = new Task ({
        name : requestBody.name
    })
    /* 
        .then accepts 2 parameters
            -first parameter (savedTask) stores the result object; if we have successfully saved the object
            -second parameter stores the error object, should there be one
    
    */
    return newTask.save().then((savedTask, error) => {
        if(error){
            console.log(error)
            return false
        } else {
            return savedTask
        }
    })
}


module.exports.deleteTask = (taskId) => {
    return Task.findByIdAndRemove(taskId).then((removedTask, error) => {
        if (error) {
            console.log(error);
            return false
        } else {
            return removedTask
        }
    })
}

/* UPDATE task - 2 parameters
    1. to find the Id (findById())
    2. replace the tasks name returned from the database with the name property of the requestBody
    3. save the task

*/

/* module.exports.updateTask = (Id) => {
    return Task.findById(Id).then((savedTask, error) => {
        if(error){
            console.log(error)
            return false
        } else {
            return savedTask
        }
    })
} */


module.exports.updateTask = ( taskId, requestBody ) =>{
    
	return Task.findById(taskId).then((result, error) => {
        console.log(result)
		if(error){
			console.log(error)
			return false
		} else {
			result.name = requestBody.name;

			return result.save().then((updateTask, error)=>{
                console.log(updateTask)
				if (error) {
					console.log(error)
					return false
				}else{
					return updateTask
				}
			})
		}
	})
}




// ACTIVITY
    /* 
    1 get a specific task in the database
    BUSINESS LOGIC
    "/:id"
    -find the id (findById())
    -return it as a response
    */
    module.exports.getTask = (taskId) => {
        return Task.findById(taskId).then(result =>{
            return result;
        })
    }

   
   /* 
   2.) update the status of a task
   BUSINESS LOGIC
   "/:id/complete"
   -find the ID of the task
   -change the value of the status into "complete"   using the requestBody
   
   */

   module.exports.getTask = (taskId) => {
    return Task.findById(taskId).then(result =>{
        return result;
    })
    }

   module.exports.updateStatus = ( taskId, requestBody ) =>{
    console.log(taskId)
	return Task.findById(taskId).then((result, error) => {
        console.log(result)
		if(error){
			console.log(error)
			return false
		} else {
            console.log(result.status)
			result.status = requestBody.status;
            console.log(requestBody.status)
			return result.save().then((updateTask, error)=>{
                console.log(updateTask)
				if (error) {
					console.log(error)
		 			return false
				}else{
					return updateTask
				}
			})
		}
	})
}
















/* module.exports.updateStatus = ( taskId, requestBody ) =>{
 console.log(taskId)
 return Task.findById(taskId).then((result, error) => {
     console.log(result)
     if(error){
         console.log(error)
         return false
     } else {
         console.log(result.status)
         result.status.type = requestBody.status;

         return result.save().then((updateTask, error)=>{
             console.log(updateTask)
             if (error) {
                 console.log(error)
                  return false
             }else{
                 return updateTask
             }
         })
     }
 })
} */
  
  
  
  
  
  
  /* module.exports.updateStatus = (taskId, requestBody) => {
   return Task.findById(taskId).then((result, error) =>{
       console.log(taskId)
       console.log(result)
       if(error){
           console.log(error)
           return false
       } else {
           console.log(result)
       }
   })
} */




