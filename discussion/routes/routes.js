// ROUTES - contains all the endpoints for the application; instead of putting a lot of routes in the index.js that we have we separate them in other file named routes.js (databaseRoutes.js i.e. taskRoute.js)

const express = require ("express")

// creates a router instance that functions as a middleware and routing system; this allows access to HTTP method middlewares that makes it easier to create routes
const router = express.Router();

// lets us use the controllers function
const taskController = require("../controllers/controllers.js");
const Task = require("../models/task.js");
// route to get all task
router.get("/", (req, res)=>{
    // taskController - a controller file that exists in the repo
    // getAlltask is a function that is encoded inside the taskController file
    // use.then to wait for the getAllTasks function to be executed before proceeding to the statements (res.send(result))
    taskController.getAllTasks().then(result => res.send(result))
})

// routes for creating a task
router.post("/", (req, res) => {
    taskController.createTask(req.body).then(result => res.send (result));
})


// WILDCARD - if tagged with a parameter, the app will automatically look for the parameter that is in the URL
router.delete("/:id", // (/:id or any key - this is what we call Wildcard)
    (req, res) => {
     // URL parameter values are accesed through the request objects "params" property; the specified parameter/property of the object will match the URL parameter endpoint
    taskController.deleteTask(req.params.id).then(result => res.send (result))
}) 

/* router.put("/:id", (req, res) =>{
    taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
}) */

router.put("/:id", (req, res)=>{
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})


// ACTIVITY
// 1
router.get("/:id", (req, res) =>{
    taskController.getTask(req.params.id, req.body).then(result => res.send(result));
})
// 2
router.put("/:id/complete", (req, res) =>{
    taskController.updateStatus(req.params.id, req.body).then(result => res.send(result));
})



//module.exports is a way for the JS to treat the value/file as a package that can be imported/used by other files
module.exports = router;




